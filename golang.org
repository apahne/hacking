#+TITLE: Golang

* Setting Up GOPATH

The ~GOPATH~ environment variable specifies the location of your workspace. If no ~GOPATH~ is set, it is assumed to be ~$HOME/go~ on Unix systems and ~%USERPROFILE%\go~ on Windows. If you want to use a custom location as your workspace, you can set the ~GOPATH~ environment variable. 




* Working With Modules

see [[https://github.com/golang/go/wiki/Modules][Go Wiki - Modules]]



* GoLand

- does not support SettingsRepository out of the box; install "Settings Repository" Plugin
