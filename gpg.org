#+TITLE: GPG

* Generate a new GPG key

#+begin_src shell :results verbatim :exports results
gpg --gen-key
#+end_src

It is advised to 

* Create Revocation certificate

#+begin_src shell :results verbatim :exports results
gpg --output gpg-revoke.asc --gen-revoke [KEY_ID]
#+end_src


* Re-new GPG identity, e.g. key expired

- create a new key
- revoke old GPG key

** git-crypt

- identify all repositories
   find . -name ".git-crypt" -type d

- add new trusted user
- revoke old trusted user


git-crypt add-gpg-user [NEW_KEY_ID]

repos
- org
- journal
- solcom
- dotfiles-secret


** gopass 

Add colaborator:

#+begin_src shell :results verbatim :exports results
gopass recipients add KEY
gopass sync
#+end_src

Remove the old colaborator before removing the key from the GPG keyring.
Then sync again


** homenet automation

Follow instructions in ansible role README



* Extract private key and import on different machine

- Identify your private key by running 

#+begin_src shell :results verbatim :exports results
gpg --list-secret-keys 
#+end_src

You need the ID of your private key (second column)

- Run this command to export your key: 

#+begin_src shell :results verbatim :exports results
gpg --export-secret-keys $ID > my-private-key.asc
#+end_src

- Copy the key to the other machine (scp is your friend)

- To import the key, run 

#+begin_src shell :results verbatim :exports results
gpg --allow-secret-key-import --import my-private-key.asc
#+end_src


* Export (And Import) Everything 

Export secret keys, public keys and ownertrust:

#+begin_src shell :results verbatim :exports results
gpg --armor --export-secret-keys > gpg-private-keys.asc
gpg --armor --export > gpg-public-keys.asc
gpg --export-ownertrust > gpg-ownertrust.asc
#+end_src

Reimport everything, for example on a new machine:

#+begin_src shell :results verbatim :exports results
gpg --import --allow-secret-key-import gpg-private-keys.asc
gpg --import gpg-public-keys.asc
gpg --import-ownertrust gpg-ownertrust.asc
#+end_src



* Setting Trust On A Key

#+begin_src shell :results verbatim :exports results
gpg --edit-key [KEY_id]
#+end_src

The gpg shell will open

#+begin_src
trust
#+end_src
